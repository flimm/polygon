Description
-----------

Draw a bunch of lines with your mouse in the window, and click `Straighten`. If
the lines roughly form one or more polygons, those polygons will appear with
straight edges.

This program was completed as a piece of coursework. With the permission of the
professor, I am sharing it here to anyone who may be interested.

Installation
------------

To compile the program in a bash shell, run:

    $ javac daviddlowe/polygon/Application.java

To run the program, run:

    $ java daviddlowe.polygon.Application

